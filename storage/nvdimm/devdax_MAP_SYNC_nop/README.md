# storage/nvdimm/devdax_MAP_SYNC_nop

Storage: nvdimm devdax MAP_SYNC nop test, BZ1568236

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
