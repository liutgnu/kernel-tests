# storage/block/RHEL_2407_reduce_thread_of_multipath

Storage: reduce or prevent thousands threads when multipath -ll is run

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
